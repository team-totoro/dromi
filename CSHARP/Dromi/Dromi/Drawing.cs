﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dromi
{
    public class Drawing
    {
        List<PaintChunck> paintChuncks;
        public Size size = new Size();
        private Bitmap bmp;
        public Drawing(Size _size)
        {
            paintChuncks = new List<PaintChunck>();
        }
        public void AddChunck(PaintChunck p)
        {
            paintChuncks.Add(p);
        }
        public void AddToLastChunck(Point p)
        {
            paintChuncks.Last().AddPoint(p);
        }
        public void Draw(PaintEventArgs p)
        {
            if (paintChuncks.Count() >= 1)
            {
                foreach (PaintChunck ptc in paintChuncks)
                {
                    ptc.Draw(p);
                }
            }
        }
        public bool isEmpty()
        {
            if(paintChuncks.Count < 1 ){
                return true;
            }
            else
            {
                return false;
            }
        }
        public void Clear()
        {
            paintChuncks.Clear();
        }
        public void DeleteLast()
        {
            if (paintChuncks.Count >= 1)
            {
                paintChuncks.RemoveAt(paintChuncks.Count -1);
            }
        }
        public void Save(PictureBox pbx)
        {
            bmp = new Bitmap(pbx.Width, pbx.Height);

            Graphics g = Graphics.FromImage(bmp);

            foreach (PaintChunck pt in paintChuncks)
            {
                pt.Save(g);
            }
            //string filename = DateTime.Now.Year + "_" + +DateTime.Now.Month + "_" + DateTime.Now.Day + "_" + DateTime.Now.Minute;
            bmp.Save(@"C:\TEST\Dromi\"+DateTime.Now.ToString("MM_dd_yyyy__HH_mm")   +".Png", ImageFormat.Png);
            //bmp.Save(@"C:\Dromi\Dromi.Png", ImageFormat.Png);
            bmp.Dispose();
        }
    }
}
