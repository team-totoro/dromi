﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dromi
{
    public class FreeHand:PaintTool
    {
        public FreeHand(Pen _pen,Drawing draw):base(_pen,"FreeHand",draw)
        {

        }
        public override void MouseDown(Point mousePosition)
        {
            PaintChunck c = new PaintChunck(thePen,false);
            draw.AddChunck(c);
            draw.AddToLastChunck(mousePosition);
        }
        public override void MouseUp(Point mousePosition)
        {
            draw.AddToLastChunck(mousePosition);
        }
        public override void TmrTick(Point mousePosition)
        { 
            draw.AddToLastChunck(mousePosition);
        }

    }
}
