﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dromi
{
    public class PaintChunck
    {
        public List<Point> points;
        public Pen pen;
        public bool dotted;
        public PaintChunck(Pen _pen, bool _dotted)
        {
            dotted = _dotted;
            pen = _pen;
            points = new List<Point>();
        }
        public void AddPoint(Point p)
        {
            points.Add(p);
        }
        public void Draw(PaintEventArgs p)
        {
            Point previous = new Point(-9999, -9999);
            if (points.Count > 1)
            {
                foreach (Point pt in points)
                {
                    if (previous == new Point(-9999, -9999))
                    {
                        //C'est un point unique, on ne peut pas dessiner une ligne avec un seul point 
                    }
                    else
                    {
                        if (dotted)
                        {
                            //on ne mets pas les lignes entre pour du coup avoir un effet pointillé
                        }
                        else
                        {
                            p.Graphics.DrawLine(pen, previous, pt);
                        }

                        p.Graphics.FillEllipse(new SolidBrush(pen.Color), new Rectangle(new Point(pt.X - Convert.ToInt32(pen.Width) / 2, pt.Y - Convert.ToInt32(pen.Width) / 2), new Size(Convert.ToInt32(pen.Width), Convert.ToInt32(pen.Width))));
                    }
                    previous = pt;

                }
            }
        }
        public void Save(Graphics g)
        {
            Point previous = new Point(-9999, -9999);
            if (points.Count > 1)
            {
                foreach (Point pt in points)
                {
                    if (previous == new Point(-9999, -9999))
                    {
                        //C'est un point unique, on ne peut pas dessiner une ligne avec un seul point 
                    }
                    else
                    {
                        if (dotted)
                        {
                            //on ne mets pas les lignes entre pour du coup avoir un effet pointillé
                        }
                        else
                        {
                            g.DrawLine(pen, previous, pt);
                        }
                        g.FillEllipse(new SolidBrush(pen.Color), new Rectangle(new Point(pt.X - Convert.ToInt32(pen.Width) / 2, pt.Y - Convert.ToInt32(pen.Width) / 2), new Size(Convert.ToInt32(pen.Width), Convert.ToInt32(pen.Width))));
                    }
                    previous = pt;

                }
            }
        }

    }
}
