﻿namespace Dromi
{
    partial class PaintForm
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PbxDrawing = new System.Windows.Forms.PictureBox();
            this.btnFreeDraw = new System.Windows.Forms.Button();
            this.btnLines = new System.Windows.Forms.Button();
            this.btnShapes = new System.Windows.Forms.Button();
            this.btnColor1 = new System.Windows.Forms.Button();
            this.btnColor3 = new System.Windows.Forms.Button();
            this.btnColor7 = new System.Windows.Forms.Button();
            this.btnColor5 = new System.Windows.Forms.Button();
            this.btnColor15 = new System.Windows.Forms.Button();
            this.btnColor13 = new System.Windows.Forms.Button();
            this.btnColor11 = new System.Windows.Forms.Button();
            this.btnColor9 = new System.Windows.Forms.Button();
            this.btnColor16 = new System.Windows.Forms.Button();
            this.btnColor14 = new System.Windows.Forms.Button();
            this.btnColor12 = new System.Windows.Forms.Button();
            this.btnColor10 = new System.Windows.Forms.Button();
            this.btnColor8 = new System.Windows.Forms.Button();
            this.btnColor6 = new System.Windows.Forms.Button();
            this.btnColor4 = new System.Windows.Forms.Button();
            this.btnColor2 = new System.Windows.Forms.Button();
            this.btnUpload = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnEreaser = new System.Windows.Forms.Button();
            this.tmrDrawing = new System.Windows.Forms.Timer(this.components);
            this.btnClear = new System.Windows.Forms.Button();
            this.nupWidth = new System.Windows.Forms.NumericUpDown();
            this.pnlTopMain = new System.Windows.Forms.Panel();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnDotted = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.PbxDrawing)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupWidth)).BeginInit();
            this.pnlTopMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // PbxDrawing
            // 
            this.PbxDrawing.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(41)))), ((int)(((byte)(41)))));
            this.PbxDrawing.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PbxDrawing.Location = new System.Drawing.Point(12, 62);
            this.PbxDrawing.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.PbxDrawing.Name = "PbxDrawing";
            this.PbxDrawing.Size = new System.Drawing.Size(1440, 720);
            this.PbxDrawing.TabIndex = 0;
            this.PbxDrawing.TabStop = false;
            this.PbxDrawing.Click += new System.EventHandler(this.PbxDrawing_Click);
            this.PbxDrawing.Paint += new System.Windows.Forms.PaintEventHandler(this.PbxDrawing_Paint);
            this.PbxDrawing.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PbxDrawing_MouseDown);
            this.PbxDrawing.MouseUp += new System.Windows.Forms.MouseEventHandler(this.PbxDrawing_MouseUp);
            // 
            // btnFreeDraw
            // 
            this.btnFreeDraw.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(41)))), ((int)(((byte)(41)))));
            this.btnFreeDraw.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnFreeDraw.Font = new System.Drawing.Font("Microsoft YaHei UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFreeDraw.ForeColor = System.Drawing.Color.White;
            this.btnFreeDraw.Location = new System.Drawing.Point(12, 13);
            this.btnFreeDraw.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnFreeDraw.Name = "btnFreeDraw";
            this.btnFreeDraw.Size = new System.Drawing.Size(87, 37);
            this.btnFreeDraw.TabIndex = 1;
            this.btnFreeDraw.Text = "Free Draw";
            this.btnFreeDraw.UseVisualStyleBackColor = false;
            this.btnFreeDraw.Click += new System.EventHandler(this.btnFreeDraw_Click);
            // 
            // btnLines
            // 
            this.btnLines.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(41)))), ((int)(((byte)(41)))));
            this.btnLines.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnLines.Font = new System.Drawing.Font("Microsoft YaHei UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLines.ForeColor = System.Drawing.Color.White;
            this.btnLines.Location = new System.Drawing.Point(105, 13);
            this.btnLines.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnLines.Name = "btnLines";
            this.btnLines.Size = new System.Drawing.Size(87, 37);
            this.btnLines.TabIndex = 3;
            this.btnLines.Text = "Lines";
            this.btnLines.UseVisualStyleBackColor = false;
            // 
            // btnShapes
            // 
            this.btnShapes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(41)))), ((int)(((byte)(41)))));
            this.btnShapes.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnShapes.Font = new System.Drawing.Font("Microsoft YaHei UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnShapes.ForeColor = System.Drawing.Color.White;
            this.btnShapes.Location = new System.Drawing.Point(291, 13);
            this.btnShapes.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnShapes.Name = "btnShapes";
            this.btnShapes.Size = new System.Drawing.Size(87, 37);
            this.btnShapes.TabIndex = 4;
            this.btnShapes.Text = "Shapes";
            this.btnShapes.UseVisualStyleBackColor = false;
            // 
            // btnColor1
            // 
            this.btnColor1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(31)))), ((int)(((byte)(63)))));
            this.btnColor1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnColor1.Font = new System.Drawing.Font("Microsoft YaHei UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnColor1.ForeColor = System.Drawing.Color.White;
            this.btnColor1.Location = new System.Drawing.Point(1467, 71);
            this.btnColor1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnColor1.Name = "btnColor1";
            this.btnColor1.Size = new System.Drawing.Size(40, 40);
            this.btnColor1.TabIndex = 7;
            this.btnColor1.UseVisualStyleBackColor = false;
            this.btnColor1.Click += new System.EventHandler(this.ChangeColor);
            // 
            // btnColor3
            // 
            this.btnColor3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(116)))), ((int)(((byte)(217)))));
            this.btnColor3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnColor3.Font = new System.Drawing.Font("Microsoft YaHei UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnColor3.ForeColor = System.Drawing.Color.White;
            this.btnColor3.Location = new System.Drawing.Point(1467, 119);
            this.btnColor3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnColor3.Name = "btnColor3";
            this.btnColor3.Size = new System.Drawing.Size(40, 40);
            this.btnColor3.TabIndex = 8;
            this.btnColor3.UseVisualStyleBackColor = false;
            this.btnColor3.Click += new System.EventHandler(this.ChangeColor);
            // 
            // btnColor7
            // 
            this.btnColor7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.btnColor7.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnColor7.Font = new System.Drawing.Font("Microsoft YaHei UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnColor7.ForeColor = System.Drawing.Color.White;
            this.btnColor7.Location = new System.Drawing.Point(1467, 215);
            this.btnColor7.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnColor7.Name = "btnColor7";
            this.btnColor7.Size = new System.Drawing.Size(40, 40);
            this.btnColor7.TabIndex = 10;
            this.btnColor7.UseVisualStyleBackColor = false;
            this.btnColor7.Click += new System.EventHandler(this.ChangeColor);
            // 
            // btnColor5
            // 
            this.btnColor5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.btnColor5.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnColor5.Font = new System.Drawing.Font("Microsoft YaHei UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnColor5.ForeColor = System.Drawing.Color.White;
            this.btnColor5.Location = new System.Drawing.Point(1467, 167);
            this.btnColor5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnColor5.Name = "btnColor5";
            this.btnColor5.Size = new System.Drawing.Size(40, 40);
            this.btnColor5.TabIndex = 9;
            this.btnColor5.UseVisualStyleBackColor = false;
            this.btnColor5.Click += new System.EventHandler(this.ChangeColor);
            // 
            // btnColor15
            // 
            this.btnColor15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(220)))), ((int)(((byte)(0)))));
            this.btnColor15.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnColor15.Font = new System.Drawing.Font("Microsoft YaHei UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnColor15.ForeColor = System.Drawing.Color.White;
            this.btnColor15.Location = new System.Drawing.Point(1467, 407);
            this.btnColor15.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnColor15.Name = "btnColor15";
            this.btnColor15.Size = new System.Drawing.Size(40, 40);
            this.btnColor15.TabIndex = 14;
            this.btnColor15.UseVisualStyleBackColor = false;
            this.btnColor15.Click += new System.EventHandler(this.ChangeColor);
            // 
            // btnColor13
            // 
            this.btnColor13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(255)))), ((int)(((byte)(112)))));
            this.btnColor13.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnColor13.Font = new System.Drawing.Font("Microsoft YaHei UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnColor13.ForeColor = System.Drawing.Color.White;
            this.btnColor13.Location = new System.Drawing.Point(1467, 359);
            this.btnColor13.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnColor13.Name = "btnColor13";
            this.btnColor13.Size = new System.Drawing.Size(40, 40);
            this.btnColor13.TabIndex = 13;
            this.btnColor13.UseVisualStyleBackColor = false;
            this.btnColor13.Click += new System.EventHandler(this.ChangeColor);
            // 
            // btnColor11
            // 
            this.btnColor11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(204)))), ((int)(((byte)(64)))));
            this.btnColor11.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnColor11.Font = new System.Drawing.Font("Microsoft YaHei UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnColor11.ForeColor = System.Drawing.Color.White;
            this.btnColor11.Location = new System.Drawing.Point(1467, 311);
            this.btnColor11.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnColor11.Name = "btnColor11";
            this.btnColor11.Size = new System.Drawing.Size(40, 40);
            this.btnColor11.TabIndex = 12;
            this.btnColor11.UseVisualStyleBackColor = false;
            this.btnColor11.Click += new System.EventHandler(this.ChangeColor);
            // 
            // btnColor9
            // 
            this.btnColor9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(153)))), ((int)(((byte)(112)))));
            this.btnColor9.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnColor9.Font = new System.Drawing.Font("Microsoft YaHei UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnColor9.ForeColor = System.Drawing.Color.White;
            this.btnColor9.Location = new System.Drawing.Point(1467, 263);
            this.btnColor9.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnColor9.Name = "btnColor9";
            this.btnColor9.Size = new System.Drawing.Size(40, 40);
            this.btnColor9.TabIndex = 11;
            this.btnColor9.UseVisualStyleBackColor = false;
            this.btnColor9.Click += new System.EventHandler(this.ChangeColor);
            // 
            // btnColor16
            // 
            this.btnColor16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(221)))), ((int)(((byte)(221)))));
            this.btnColor16.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnColor16.Font = new System.Drawing.Font("Microsoft YaHei UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnColor16.ForeColor = System.Drawing.Color.White;
            this.btnColor16.Location = new System.Drawing.Point(1513, 407);
            this.btnColor16.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnColor16.Name = "btnColor16";
            this.btnColor16.Size = new System.Drawing.Size(40, 40);
            this.btnColor16.TabIndex = 22;
            this.btnColor16.UseVisualStyleBackColor = false;
            this.btnColor16.Click += new System.EventHandler(this.ChangeColor);
            // 
            // btnColor14
            // 
            this.btnColor14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.btnColor14.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnColor14.Font = new System.Drawing.Font("Microsoft YaHei UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnColor14.ForeColor = System.Drawing.Color.White;
            this.btnColor14.Location = new System.Drawing.Point(1513, 359);
            this.btnColor14.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnColor14.Name = "btnColor14";
            this.btnColor14.Size = new System.Drawing.Size(40, 40);
            this.btnColor14.TabIndex = 21;
            this.btnColor14.UseVisualStyleBackColor = false;
            this.btnColor14.Click += new System.EventHandler(this.ChangeColor);
            // 
            // btnColor12
            // 
            this.btnColor12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.btnColor12.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnColor12.Font = new System.Drawing.Font("Microsoft YaHei UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnColor12.ForeColor = System.Drawing.Color.White;
            this.btnColor12.Location = new System.Drawing.Point(1513, 311);
            this.btnColor12.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnColor12.Name = "btnColor12";
            this.btnColor12.Size = new System.Drawing.Size(40, 40);
            this.btnColor12.TabIndex = 20;
            this.btnColor12.UseVisualStyleBackColor = false;
            this.btnColor12.Click += new System.EventHandler(this.ChangeColor);
            // 
            // btnColor10
            // 
            this.btnColor10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(13)))), ((int)(((byte)(201)))));
            this.btnColor10.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnColor10.Font = new System.Drawing.Font("Microsoft YaHei UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnColor10.ForeColor = System.Drawing.Color.White;
            this.btnColor10.Location = new System.Drawing.Point(1513, 263);
            this.btnColor10.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnColor10.Name = "btnColor10";
            this.btnColor10.Size = new System.Drawing.Size(40, 40);
            this.btnColor10.TabIndex = 19;
            this.btnColor10.UseVisualStyleBackColor = false;
            this.btnColor10.Click += new System.EventHandler(this.ChangeColor);
            // 
            // btnColor8
            // 
            this.btnColor8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(18)))), ((int)(((byte)(190)))));
            this.btnColor8.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnColor8.Font = new System.Drawing.Font("Microsoft YaHei UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnColor8.ForeColor = System.Drawing.Color.White;
            this.btnColor8.Location = new System.Drawing.Point(1513, 215);
            this.btnColor8.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnColor8.Name = "btnColor8";
            this.btnColor8.Size = new System.Drawing.Size(40, 40);
            this.btnColor8.TabIndex = 18;
            this.btnColor8.UseVisualStyleBackColor = false;
            this.btnColor8.Click += new System.EventHandler(this.ChangeColor);
            // 
            // btnColor6
            // 
            this.btnColor6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(133)))), ((int)(((byte)(20)))), ((int)(((byte)(75)))));
            this.btnColor6.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnColor6.Font = new System.Drawing.Font("Microsoft YaHei UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnColor6.ForeColor = System.Drawing.Color.White;
            this.btnColor6.Location = new System.Drawing.Point(1513, 167);
            this.btnColor6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnColor6.Name = "btnColor6";
            this.btnColor6.Size = new System.Drawing.Size(40, 40);
            this.btnColor6.TabIndex = 17;
            this.btnColor6.UseVisualStyleBackColor = false;
            this.btnColor6.Click += new System.EventHandler(this.ChangeColor);
            // 
            // btnColor4
            // 
            this.btnColor4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(65)))), ((int)(((byte)(54)))));
            this.btnColor4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnColor4.Font = new System.Drawing.Font("Microsoft YaHei UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnColor4.ForeColor = System.Drawing.Color.White;
            this.btnColor4.Location = new System.Drawing.Point(1513, 119);
            this.btnColor4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnColor4.Name = "btnColor4";
            this.btnColor4.Size = new System.Drawing.Size(40, 40);
            this.btnColor4.TabIndex = 16;
            this.btnColor4.UseVisualStyleBackColor = false;
            this.btnColor4.Click += new System.EventHandler(this.ChangeColor);
            // 
            // btnColor2
            // 
            this.btnColor2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(133)))), ((int)(((byte)(27)))));
            this.btnColor2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnColor2.Font = new System.Drawing.Font("Microsoft YaHei UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnColor2.ForeColor = System.Drawing.Color.White;
            this.btnColor2.Location = new System.Drawing.Point(1513, 71);
            this.btnColor2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnColor2.Name = "btnColor2";
            this.btnColor2.Size = new System.Drawing.Size(40, 40);
            this.btnColor2.TabIndex = 15;
            this.btnColor2.UseVisualStyleBackColor = false;
            this.btnColor2.Click += new System.EventHandler(this.ChangeColor);
            // 
            // btnUpload
            // 
            this.btnUpload.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(41)))), ((int)(((byte)(41)))));
            this.btnUpload.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnUpload.Font = new System.Drawing.Font("Microsoft YaHei UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpload.ForeColor = System.Drawing.Color.White;
            this.btnUpload.Location = new System.Drawing.Point(1298, 13);
            this.btnUpload.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnUpload.Name = "btnUpload";
            this.btnUpload.Size = new System.Drawing.Size(87, 37);
            this.btnUpload.TabIndex = 23;
            this.btnUpload.Text = "Upload";
            this.btnUpload.UseVisualStyleBackColor = false;
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(41)))), ((int)(((byte)(41)))));
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSave.Font = new System.Drawing.Font("Microsoft YaHei UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Location = new System.Drawing.Point(1391, 13);
            this.btnSave.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(87, 37);
            this.btnSave.TabIndex = 24;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnEreaser
            // 
            this.btnEreaser.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(41)))), ((int)(((byte)(41)))));
            this.btnEreaser.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnEreaser.Font = new System.Drawing.Font("Microsoft YaHei UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEreaser.ForeColor = System.Drawing.Color.White;
            this.btnEreaser.Location = new System.Drawing.Point(384, 13);
            this.btnEreaser.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnEreaser.Name = "btnEreaser";
            this.btnEreaser.Size = new System.Drawing.Size(87, 37);
            this.btnEreaser.TabIndex = 25;
            this.btnEreaser.Text = "Ereaser";
            this.btnEreaser.UseVisualStyleBackColor = false;
            // 
            // tmrDrawing
            // 
            this.tmrDrawing.Interval = 10;
            this.tmrDrawing.Tick += new System.EventHandler(this.tmrDrawing_Tick);
            // 
            // btnClear
            // 
            this.btnClear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(41)))), ((int)(((byte)(41)))));
            this.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnClear.Font = new System.Drawing.Font("Microsoft YaHei UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.ForeColor = System.Drawing.Color.White;
            this.btnClear.Location = new System.Drawing.Point(1466, 489);
            this.btnClear.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(87, 37);
            this.btnClear.TabIndex = 26;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // nupWidth
            // 
            this.nupWidth.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(41)))), ((int)(((byte)(41)))));
            this.nupWidth.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.nupWidth.Font = new System.Drawing.Font("Microsoft YaHei UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nupWidth.ForeColor = System.Drawing.Color.White;
            this.nupWidth.Location = new System.Drawing.Point(1466, 454);
            this.nupWidth.Maximum = new decimal(new int[] {
            25,
            0,
            0,
            0});
            this.nupWidth.Name = "nupWidth";
            this.nupWidth.Size = new System.Drawing.Size(87, 28);
            this.nupWidth.TabIndex = 27;
            this.nupWidth.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.nupWidth.ValueChanged += new System.EventHandler(this.WidthModification);
            this.nupWidth.KeyUp += new System.Windows.Forms.KeyEventHandler(this.WidthModification);
            // 
            // pnlTopMain
            // 
            this.pnlTopMain.Controls.Add(this.btnExit);
            this.pnlTopMain.Controls.Add(this.btnDotted);
            this.pnlTopMain.Controls.Add(this.btnFreeDraw);
            this.pnlTopMain.Controls.Add(this.btnLines);
            this.pnlTopMain.Controls.Add(this.btnUpload);
            this.pnlTopMain.Controls.Add(this.btnSave);
            this.pnlTopMain.Controls.Add(this.btnEreaser);
            this.pnlTopMain.Controls.Add(this.btnShapes);
            this.pnlTopMain.Location = new System.Drawing.Point(0, 0);
            this.pnlTopMain.Name = "pnlTopMain";
            this.pnlTopMain.Size = new System.Drawing.Size(1560, 64);
            this.pnlTopMain.TabIndex = 29;
            this.pnlTopMain.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlTopMain_MouseDown);
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(65)))), ((int)(((byte)(54)))));
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnExit.Font = new System.Drawing.Font("Microsoft YaHei UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(221)))), ((int)(((byte)(221)))));
            this.btnExit.Location = new System.Drawing.Point(1484, 13);
            this.btnExit.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(64, 37);
            this.btnExit.TabIndex = 30;
            this.btnExit.Text = "X";
            this.btnExit.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnDotted
            // 
            this.btnDotted.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(41)))), ((int)(((byte)(41)))));
            this.btnDotted.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnDotted.Font = new System.Drawing.Font("Microsoft YaHei UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDotted.ForeColor = System.Drawing.Color.White;
            this.btnDotted.Location = new System.Drawing.Point(198, 13);
            this.btnDotted.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnDotted.Name = "btnDotted";
            this.btnDotted.Size = new System.Drawing.Size(87, 37);
            this.btnDotted.TabIndex = 29;
            this.btnDotted.Text = "Dotted";
            this.btnDotted.UseVisualStyleBackColor = false;
            this.btnDotted.Click += new System.EventHandler(this.btnDotted_Click);
            // 
            // btnBack
            // 
            this.btnBack.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(41)))), ((int)(((byte)(41)))));
            this.btnBack.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnBack.Font = new System.Drawing.Font("Microsoft YaHei UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBack.ForeColor = System.Drawing.Color.White;
            this.btnBack.Location = new System.Drawing.Point(1467, 534);
            this.btnBack.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(87, 37);
            this.btnBack.TabIndex = 28;
            this.btnBack.Text = "Back";
            this.btnBack.UseVisualStyleBackColor = false;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // PaintForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(31)))), ((int)(((byte)(31)))));
            this.ClientSize = new System.Drawing.Size(1560, 795);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.nupWidth);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnColor16);
            this.Controls.Add(this.btnColor14);
            this.Controls.Add(this.btnColor12);
            this.Controls.Add(this.btnColor10);
            this.Controls.Add(this.btnColor8);
            this.Controls.Add(this.btnColor6);
            this.Controls.Add(this.btnColor4);
            this.Controls.Add(this.btnColor2);
            this.Controls.Add(this.btnColor15);
            this.Controls.Add(this.btnColor13);
            this.Controls.Add(this.btnColor11);
            this.Controls.Add(this.btnColor9);
            this.Controls.Add(this.btnColor7);
            this.Controls.Add(this.btnColor5);
            this.Controls.Add(this.btnColor3);
            this.Controls.Add(this.btnColor1);
            this.Controls.Add(this.PbxDrawing);
            this.Controls.Add(this.pnlTopMain);
            this.Font = new System.Drawing.Font("Microsoft YaHei UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "PaintForm";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.PaintForm_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlTopMain_MouseDown);
            ((System.ComponentModel.ISupportInitialize)(this.PbxDrawing)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupWidth)).EndInit();
            this.pnlTopMain.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox PbxDrawing;
        private System.Windows.Forms.Button btnFreeDraw;
        private System.Windows.Forms.Button btnLines;
        private System.Windows.Forms.Button btnShapes;
        private System.Windows.Forms.Button btnColor1;
        private System.Windows.Forms.Button btnColor3;
        private System.Windows.Forms.Button btnColor7;
        private System.Windows.Forms.Button btnColor5;
        private System.Windows.Forms.Button btnColor15;
        private System.Windows.Forms.Button btnColor13;
        private System.Windows.Forms.Button btnColor11;
        private System.Windows.Forms.Button btnColor9;
        private System.Windows.Forms.Button btnColor16;
        private System.Windows.Forms.Button btnColor14;
        private System.Windows.Forms.Button btnColor12;
        private System.Windows.Forms.Button btnColor10;
        private System.Windows.Forms.Button btnColor8;
        private System.Windows.Forms.Button btnColor6;
        private System.Windows.Forms.Button btnColor4;
        private System.Windows.Forms.Button btnColor2;
        private System.Windows.Forms.Button btnUpload;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnEreaser;
        private System.Windows.Forms.Timer tmrDrawing;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.NumericUpDown nupWidth;
        private System.Windows.Forms.Panel pnlTopMain;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button btnDotted;
        private System.Windows.Forms.Button btnExit;
    }
}

