﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dromi
{
    public partial class PaintForm : Form
    {
        List<PaintTool> PaintTools = new List<PaintTool>();
        Drawing D;
        #region Form Move

        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern bool ReleaseCapture();

        #endregion
        public PaintForm()
        {
            InitializeComponent();
            D = new Drawing(PbxDrawing.Size);
            PaintTools.Add(new FreeHand(new Pen(Color.Red, 5), D));
            PaintTools[0].selected = true;
        }

        private void PaintForm_Load(object sender, EventArgs e)
        {

        }

        private void PbxDrawing_Click(object sender, EventArgs e)
        {

        }
        private void tmrDrawing_Tick(object sender, EventArgs e)
        {
            foreach (PaintTool p in PaintTools)
            {
                if (p.selected)
                {
                    p.TmrTick(PbxDrawing.PointToClient(MousePosition));
                }
            }
            PbxDrawing.Invalidate();
        }
        private void PbxDrawing_MouseDown(object sender, MouseEventArgs e)
        {
            //le probleme avec le numeric up and down c'est que si le focus ne change pas la value non plus
            PbxDrawing.Focus();

            tmrDrawing.Start();
            foreach (PaintTool p in PaintTools)
            {
                if (p.selected)
                {
                    p.MouseDown(PbxDrawing.PointToClient(MousePosition));
                }
            }
        }
        private void PbxDrawing_MouseUp(object sender, MouseEventArgs e)
        {
            tmrDrawing.Stop();
            foreach (PaintTool p in PaintTools)
            {
                if (p.selected)
                {
                    p.MouseDown(PbxDrawing.PointToClient(MousePosition));
                }
            }
        }

        private void PbxDrawing_Paint(object sender, PaintEventArgs e)
        {
            if (!D.isEmpty())
            {
                D.Draw(e);
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            D.Clear();
            PbxDrawing.Invalidate();
        }



        private void ChangeColor(object sender, EventArgs e)
        {
            foreach (PaintTool p in PaintTools)
            {
                if (p.selected && sender is Button)
                {
                    Button btn = sender as Button;
                    p.ChangeColor(btn.BackColor);
                }
            }
        }


        private void WidthModification(object sender, EventArgs e)
        {
            foreach (PaintTool p in PaintTools)
            {
                Button btn = sender as Button;
                p.ChangeWidth(Convert.ToInt32(nupWidth.Value));
            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            D.DeleteLast();
            PbxDrawing.Invalidate();
        }

        private void pnlTopMain_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void lblExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        
  
        private void btnSave_Click(object sender, EventArgs e)
        {
            D.Save(PbxDrawing);
        }

        private void btnDotted_Click(object sender, EventArgs e)
        {
            foreach (PaintTool p in PaintTools)
            {
                if (p is Dotted)
                {
                    if (p.selected)
                    {
                        p.selected = false;
                    }
                    else
                    {
                        p.selected = true;
                    }
                }
                
            }
        }
        private void btnFreeDraw_Click(object sender, EventArgs e)
        {
            foreach (PaintTool p in PaintTools)
            {
                if (p is FreeHand)
                {
                    if (p.selected)
                    {
                        p.selected = false;
                    }
                    else
                    {
                        p.selected = true;
                    }
                }
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
