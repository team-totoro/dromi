﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dromi
{
    public abstract class PaintTool
    {
        public bool selected;
        public string name;
        public int width;
        public Color color;
        protected Pen thePen;
        protected Drawing draw;

        public PaintTool(Pen _pen,string _name,Drawing _draw)
        {
            thePen = _pen;
            name = _name;
            selected = false;
            draw = _draw;
        }
        public void ChangeColor(Color c)
        {
            thePen = new Pen(c, thePen.Width);
        }
        public void ChangeWidth(int w)
        {
            //on empeche les gens de mettre une largeur abusée
            if (w < 1)
            {
                w = 1;
            }
            else
            {
                if (w > 25)
                {
                    w = 25;
                }
            }
            thePen = new Pen(thePen.Color, w);
        }

        /*
         * Je demande a tous les outils d'implementer eux meme ce qu'ils veulent faire quand un user clique maintiens appuyé et lache la 
        */
        public abstract void MouseDown(Point mousePosition);
        public abstract void MouseUp(Point mousePosition);
        public abstract void TmrTick(Point mousePosition);
        
    }
}
