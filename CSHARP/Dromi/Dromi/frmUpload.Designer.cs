﻿namespace Dromi
{
    partial class frmUpload
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pcbUpload = new System.Windows.Forms.PictureBox();
            this.pnlTopUpload = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.lblExit = new System.Windows.Forms.Label();
            this.tbxLegende = new System.Windows.Forms.TextBox();
            this.lblLegende = new System.Windows.Forms.Label();
            this.btnEnvoyer = new System.Windows.Forms.Button();
            this.btnRetour = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pcbUpload)).BeginInit();
            this.pnlTopUpload.SuspendLayout();
            this.SuspendLayout();
            // 
            // pcbUpload
            // 
            this.pcbUpload.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(41)))), ((int)(((byte)(41)))));
            this.pcbUpload.Location = new System.Drawing.Point(73, 76);
            this.pcbUpload.Name = "pcbUpload";
            this.pcbUpload.Size = new System.Drawing.Size(1440, 720);
            this.pcbUpload.TabIndex = 0;
            this.pcbUpload.TabStop = false;
            // 
            // pnlTopUpload
            // 
            this.pnlTopUpload.Controls.Add(this.label1);
            this.pnlTopUpload.Controls.Add(this.lblExit);
            this.pnlTopUpload.Location = new System.Drawing.Point(0, 0);
            this.pnlTopUpload.Name = "pnlTopUpload";
            this.pnlTopUpload.Size = new System.Drawing.Size(1572, 48);
            this.pnlTopUpload.TabIndex = 3;
            this.pnlTopUpload.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlTopUpload_MouseDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(3, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 25);
            this.label1.TabIndex = 1;
            this.label1.Text = "Dromi";
            // 
            // lblExit
            // 
            this.lblExit.AutoSize = true;
            this.lblExit.BackColor = System.Drawing.Color.Transparent;
            this.lblExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExit.ForeColor = System.Drawing.Color.Red;
            this.lblExit.Location = new System.Drawing.Point(1536, 9);
            this.lblExit.Name = "lblExit";
            this.lblExit.Size = new System.Drawing.Size(26, 25);
            this.lblExit.TabIndex = 0;
            this.lblExit.Text = "X";
            this.lblExit.Click += new System.EventHandler(this.lblExit_Click);
            this.lblExit.MouseEnter += new System.EventHandler(this.lblExit_MouseEnter);
            this.lblExit.MouseLeave += new System.EventHandler(this.lblExit_MouseLeave);
            // 
            // tbxLegende
            // 
            this.tbxLegende.Location = new System.Drawing.Point(94, 818);
            this.tbxLegende.Multiline = true;
            this.tbxLegende.Name = "tbxLegende";
            this.tbxLegende.Size = new System.Drawing.Size(1392, 112);
            this.tbxLegende.TabIndex = 4;
            // 
            // lblLegende
            // 
            this.lblLegende.AutoSize = true;
            this.lblLegende.ForeColor = System.Drawing.Color.White;
            this.lblLegende.Location = new System.Drawing.Point(30, 830);
            this.lblLegende.Name = "lblLegende";
            this.lblLegende.Size = new System.Drawing.Size(58, 13);
            this.lblLegende.TabIndex = 5;
            this.lblLegende.Text = "Légende : ";
            // 
            // btnEnvoyer
            // 
            this.btnEnvoyer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(41)))), ((int)(((byte)(41)))));
            this.btnEnvoyer.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnEnvoyer.Font = new System.Drawing.Font("Microsoft YaHei UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEnvoyer.ForeColor = System.Drawing.Color.White;
            this.btnEnvoyer.Location = new System.Drawing.Point(1461, 950);
            this.btnEnvoyer.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnEnvoyer.Name = "btnEnvoyer";
            this.btnEnvoyer.Size = new System.Drawing.Size(87, 37);
            this.btnEnvoyer.TabIndex = 25;
            this.btnEnvoyer.Text = "Envoyer";
            this.btnEnvoyer.UseVisualStyleBackColor = false;
            // 
            // btnRetour
            // 
            this.btnRetour.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(41)))), ((int)(((byte)(41)))));
            this.btnRetour.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnRetour.Font = new System.Drawing.Font("Microsoft YaHei UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRetour.ForeColor = System.Drawing.Color.White;
            this.btnRetour.Location = new System.Drawing.Point(12, 950);
            this.btnRetour.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnRetour.Name = "btnRetour";
            this.btnRetour.Size = new System.Drawing.Size(87, 37);
            this.btnRetour.TabIndex = 26;
            this.btnRetour.Text = "Retour";
            this.btnRetour.UseVisualStyleBackColor = false;
            // 
            // frmUpload
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(31)))), ((int)(((byte)(31)))));
            this.ClientSize = new System.Drawing.Size(1574, 1000);
            this.Controls.Add(this.btnRetour);
            this.Controls.Add(this.btnEnvoyer);
            this.Controls.Add(this.lblLegende);
            this.Controls.Add(this.tbxLegende);
            this.Controls.Add(this.pnlTopUpload);
            this.Controls.Add(this.pcbUpload);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximumSize = new System.Drawing.Size(1574, 1000);
            this.MinimumSize = new System.Drawing.Size(1574, 1000);
            this.Name = "frmUpload";
            this.Text = "frmUpload";
            ((System.ComponentModel.ISupportInitialize)(this.pcbUpload)).EndInit();
            this.pnlTopUpload.ResumeLayout(false);
            this.pnlTopUpload.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pcbUpload;
        private System.Windows.Forms.Panel pnlTopUpload;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblExit;
        private System.Windows.Forms.TextBox tbxLegende;
        private System.Windows.Forms.Label lblLegende;
        private System.Windows.Forms.Button btnEnvoyer;
        private System.Windows.Forms.Button btnRetour;
    }
}