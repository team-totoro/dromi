<?php
require "./assets/php/Scripts.php";

$username = filter_input(INPUT_POST, "username", FILTER_SANITIZE_STRING);
$email = filter_input(INPUT_POST, "email", FILTER_SANITIZE_EMAIL);
$password = filter_input(INPUT_POST, "password", FILTER_SANITIZE_STRING);
$repassword = filter_input(INPUT_POST, "repassword", FILTER_SANITIZE_STRING);

$artiste = selectArtisteById($_SESSION['idArtiste'])[0];
var_dump($artiste);
$currentEmail = $artiste['email'];
$currentUsername = $artiste['username'];

if (isset($_POST["submit"])) {
    if ($username != NULL && $email != NULL) {
        $result = checkEmail($email);
        if ($result != null && $email != $currentEmail) {
            $erreur = "Email déja utilisé";
        } else {
            if ($password != "") {
                updateProfileWithPassword($email, $username, hash("md5", $password), $_SESSION['idArtiste']);
                $artiste = selectArtisteById($_SESSION['idArtiste'])[0];
                $currentEmail = $artiste['email'];
                $currentUsername = $artiste['username'];
                header("Location: profile.php");
            } else {
                //UpdateProfile
                updateProfile($email, $username, $_SESSION['idArtiste']);
                $artiste = selectArtisteById($_SESSION['idArtiste'])[0];
                $currentEmail = $artiste['email'];
                $currentUsername = $artiste['username'];
                header("Location: profile.php");
            }
        }
    }
}
?>
<!DOCTYPE html>
<html style="height: 100vh;">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Dromi</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Actor">
    <link rel="stylesheet" href="assets/fonts/font-awesome.min.css">
    <link rel="stylesheet" href="assets/fonts/ionicons.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/css/swiper.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
</head>

<body class="bodyLoginRegister">
    <div class="login-clean">
        <form method="post">
            <h2 class="sr-only">Login Form</h2>
            <div class="illustration"><i class="icon ion-ios-navigate"></i></div>
            <div class="form-group"><input class="form-control" type="text" name="username" value="<?php echo $currentUsername ?>"></div>
            <div class="form-group"><input class="form-control" type="email" name="email" value="<?php echo $currentEmail ?>"></div>
            <div class="form-group"><input class="form-control" type="password" name="password" placeholder="Password"></div>
            <div class="form-group"><input class="form-control" type="password" name="repassword" placeholder="Repeat Password"></div>
            <div class="alert alert-danger" role="alert" <?php if (!isset($erreur)) {
                                                                echo "hidden";
                                                            } ?> name="alertWrongLogin"><?php echo $erreur ?></div>
            <div class="form-group"><button class="btn btn-primary btn-block" type="submit" name="submit">Update</button></div>
        </form>
    </div>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="assets/js/current-day.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.jquery.min.js"></script>
    <script src="assets/js/Simple-Slider.js"></script>
</body>

</html>